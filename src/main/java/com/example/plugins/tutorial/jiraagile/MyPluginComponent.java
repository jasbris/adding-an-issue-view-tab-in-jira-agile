package com.example.plugins.tutorial.jiraagile;

public interface MyPluginComponent
{
    String getName();
}